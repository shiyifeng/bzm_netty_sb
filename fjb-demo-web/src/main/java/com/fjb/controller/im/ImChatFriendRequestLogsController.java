package com.fjb.controller.im;


import java.time.LocalDateTime;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.fjb.entity.HttpCode;
import com.fjb.entity.JsonResult;
import com.fjb.pojo.im.ImChatFriendInfo;
import com.fjb.pojo.im.ImChatFriendRequestLogs;
import com.fjb.pojo.im.vo.ImChatFriendRequestLogsVo;
import com.fjb.pojo.user.SysUser;
import com.fjb.service.im.ImChatFriendRequestLogsService;
import com.fjb.service.user.SysUserService;

/**
 * <p>
 * 好友请求记录 前端控制器
 * </p>
 *
 * @author hemiao
 * @since 2020-06-02
 */
@Controller
@RequestMapping("/imChatFriendRequestLogs")
public class ImChatFriendRequestLogsController {
	
	@Autowired
	private SysUserService sysUserService;
	@Autowired
	private ImChatFriendRequestLogsService imChatFriendRequestLogsService;
	
	// 添加好友申请
	@RequestMapping(value="/addInfo")
	@ResponseBody
	public JsonResult<Boolean> addInfo(HttpServletRequest request,Integer mainUserId,
			Integer userId){
		HttpCode httpCode = HttpCode.PARAM_VERIFICATION;
		String phone = request.getParameter("phone");
		if(StringUtils.isBlank(phone)) {
			httpCode.setMsg("好友手机号不能为空");
			return new JsonResult<Boolean>(null, httpCode);
		}		
		QueryWrapper<SysUser> queryWrapper = new QueryWrapper<SysUser>();
		queryWrapper.eq("phone", phone);
		SysUser requestUser = sysUserService.getOne(queryWrapper);
		if(requestUser==null) {
			httpCode.setMsg("好友不存在");
			return new JsonResult<Boolean>(null, httpCode);
		}
		Integer requestUserId = requestUser.getId();
		String requestRemark = request.getParameter("requestRemark");
		// 查询是否添加过好友
		QueryWrapper<ImChatFriendRequestLogs> logsWrapper = new QueryWrapper<ImChatFriendRequestLogs>();
		logsWrapper.eq("user_id", userId);
		logsWrapper.eq("request_user_id", requestUserId);
		logsWrapper.eq("data_status", 1);
		ImChatFriendRequestLogs oldRequestLogs = imChatFriendRequestLogsService.getOne(logsWrapper);
		if(oldRequestLogs!=null) {
			Integer oldStatus = oldRequestLogs.getRequestStatus();
			// 请求状态     1、通过     2、已拒绝   3、申请中
			if(oldStatus.equals(3)) {
				httpCode.setMsg("好友在申请中,请耐心等待");
				return new JsonResult<Boolean>(null, httpCode);
			}
			if(oldStatus.equals(1)) {
				httpCode.setMsg("该用户已经是您的好友");
				return new JsonResult<Boolean>(null, httpCode);
			}
		}	
		ImChatFriendRequestLogs logs = new ImChatFriendRequestLogs();
		logs.setMainUserId(mainUserId);
		logs.setUserId(userId);
		logs.setRequestUserId(requestUserId);
		logs.setRequestStatus(3);
		logs.setLookStatus(2);
		logs.setCreateUserId(userId);
		logs.setCreateTime(LocalDateTime.now());
		logs.setDataStatus(1);
		if(StringUtils.isNoneBlank(requestRemark)) {
			logs.setRequestRemark(requestRemark);
		}
		boolean b = imChatFriendRequestLogsService.save(logs);
		return new JsonResult<Boolean>(b, HttpCode.SUCCESS);
	}
		
	// 查询好友请求记录
	@RequestMapping(value="/selectRequestLogsList")
	@ResponseBody
	public JsonResult<List<ImChatFriendRequestLogsVo>> selectRequestLogsList(HttpServletRequest request,
			Integer userId){
		HttpCode httpCode = HttpCode.PARAM_VERIFICATION;
		if(userId==null) {
			return new JsonResult<List<ImChatFriendRequestLogsVo>>(null, httpCode);
		}	
		List<ImChatFriendRequestLogsVo> infoList = imChatFriendRequestLogsService.selectRequestLogsList(userId);
		return new JsonResult<List<ImChatFriendRequestLogsVo>>(infoList, HttpCode.SUCCESS);
	}
	
	// 修改好友申请状态
	@RequestMapping(value="/updateRequestStatus")
	@ResponseBody
	public JsonResult<Boolean> updateRequestStatus(HttpServletRequest request,Integer requestId,
			Integer updateStatus){
		HttpCode httpCode = HttpCode.PARAM_VERIFICATION;
		if(requestId==null||updateStatus==null) {
			return new JsonResult<Boolean>(null, httpCode);
		}
		if(updateStatus.equals(1)||updateStatus.equals(2)) {
			ImChatFriendRequestLogs oldRequestLogs = imChatFriendRequestLogsService.getById(requestId);
			if(oldRequestLogs==null) {
				httpCode.setMsg("申请记录不存在");
				return new JsonResult<Boolean>(null, httpCode);
			}
			Integer oldRequestStatus = oldRequestLogs.getRequestStatus();
			if(!oldRequestStatus.equals(3)) {
				httpCode.setMsg("数据状态异常");
				return new JsonResult<Boolean>(null, httpCode);
			}
			Integer mainUserId = oldRequestLogs.getMainUserId();
			Integer userId = oldRequestLogs.getUserId();
			Integer requestUserId = oldRequestLogs.getRequestUserId();
			LocalDateTime localDateTime = LocalDateTime.now();
			ImChatFriendRequestLogs updateLogs = new ImChatFriendRequestLogs();
			updateLogs.setId(requestId);
			updateLogs.setUpdateTime(localDateTime);
			updateLogs.setUpdateUserId(userId);
			if(updateStatus.equals(1)) {
				// 好友申请通过	
				updateLogs.setRequestStatus(1);
				// 添加好友信息
				ImChatFriendInfo fi = new ImChatFriendInfo();
				fi.setMainUserId(mainUserId);
				fi.setUserId(userId);
				fi.setFriendUserId(requestUserId);
				fi.setCreateUserId(userId);
				fi.setCreateTime(localDateTime);
				return imChatFriendRequestLogsService.updateRequestStatus1(updateLogs,fi);
			}else if (updateStatus.equals(2)) {
				// 好友申请拒绝	
				updateLogs.setRequestStatus(2);
				imChatFriendRequestLogsService.updateById(updateLogs);
				return new JsonResult<Boolean>(null, HttpCode.SUCCESS);
			}
		}else {
			return new JsonResult<Boolean>(null, httpCode);
		}	
		return new JsonResult<Boolean>(null, httpCode);
	}
}
