package com.fjb.controller.im;


import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.fjb.entity.HttpCode;
import com.fjb.entity.JsonResult;
import com.fjb.pojo.im.vo.ImChatFriendInfoVo;
import com.fjb.service.im.ImChatFriendInfoService;

/**
 * <p>
 * im_chat 朋友信息表 前端控制器
 * </p>
 *
 * @author hemiao
 * @since 2020-06-02
 */
@Controller
@RequestMapping("/imChatFriendInfo")
public class ImChatFriendInfoController {
	
	@Autowired
	private ImChatFriendInfoService imChatFriendInfoService;
	
	// 查询好友列表
	@RequestMapping(value="/selectFriendList")
	@ResponseBody
	public JsonResult<List<ImChatFriendInfoVo>> selectFriendList(HttpServletRequest request,
			Integer userId){
		HttpCode httpCode = HttpCode.PARAM_VERIFICATION;
		if(userId==null) {
			return new JsonResult<List<ImChatFriendInfoVo>>(null, httpCode);
		}
		List<ImChatFriendInfoVo> infoList = imChatFriendInfoService.selectFriendList(userId);
		return new JsonResult<List<ImChatFriendInfoVo>>(infoList, HttpCode.SUCCESS);
	}
}
