package com.fjb.entity;

import java.io.Serializable;

/**
 * @Description: 分页请求对象
 * @Author: hm
 * @CreateDate: 2018/3/11 11:02
 * @Version: 1.0
 */
public class PageQuery implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -2515034900924486364L;
	
	/**
	 * 每页显示多少条
	 */
	private int pageSize = 10;

	/**
	 * 当前第几页
	 */
    private int currentPage = 1;

    public int getPageSize() {
        return pageSize;
    }

    public void setPageSize(int pageSize) {
        this.pageSize = pageSize;
    }

    public int getCurrentPage() {
        return currentPage;
    }

    public void setCurrentPage(int currentPage) {
        this.currentPage = currentPage;
    }
}
