package com.fjb.entity;

public class JsonTotalResult<T> {

	/**
	 * 返回数据
	 */
    private T dataObject;
    
    /**
     * 错误码
     */
    private Integer code;
    
    /**
     * 描述信息
     */
    private String msg;
    
    /**
     * 总数量
     */
    private Long total;
    
    public JsonTotalResult(){
    	
    }
    
    public JsonTotalResult(T dataObject,HttpCode httpCode,Long total) {
        this.dataObject = dataObject;
        this.code = httpCode.getCode();
        this.msg = httpCode.getMsg();
        this.total=total;
    }

    public T getDataObject() {
        return dataObject;
    }

    public void setDataObject(T dataObject) {
        this.dataObject = dataObject;
    }

    public Integer getCode() {
        return code;
    }

    public void setCode(Integer code) {
        this.code = code;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public Long getTotal() {
        return total;
    }

    public void setTotal(Long total) {
        this.total = total;
    }
}
